# Biodata Perserta JCC

<p>Nama : Rahmat</p>
<p>Kelas : Laravel</p>

# Link Deploy

Deploy Heroku : http://app-kasir-laravel.herokuapp.com/ (SUDAH BISA DIAKSES)

## Aplikasi Kasir Sederhana V1.0, Kelas JCC Partnership - Project Challenge (Laravel Project) Jabar Coding Camp (JCC)

Jabar Coding Camp (JCC), Aplikasi Kasir V1.0 (Laravel Project), Kelas JCC Partnership - Project Challenge (Tantangan).<br>
Anda diminta untuk membuat aplikasi kasir sederhana yang mencakup fitur:

1. Mencatat transaksi pembelian barang
2. Menampilkan daftar transaksi pembelian barang
3. Authentikasi login pengguna
4. Pengaturan data pengguna
5. Pengaturan data master produk

<b>Akun Admin :</b>

<ul>
<li>email : rahmat@gmail.com</li>
<li>password : kasiramansentosa</li>
</ul>

<b>Akun Kasir :</b>

<ul>
<li>email : hilda@gmail.com</li>
<li>password : kasiramansentosa</li>
</ul>

<ul>
<li>email : ari@gmail.com</li>
<li>password : kasiramansentosa</li>
</ul>

## Fitur Role Atau Pembatasan Menu Untuk Setiap Users

-   Admin
-   Kasir

## Fitur Aplikasi

-   Login<br>
-   Register<br>
-   Logout<br>
-   Halaman Utama<br><br>

    A. Profile<br>

-   Ubah Data (Data Profile)<br>
-   Detail Data (Data Profile)<br><br>

    B. Data Pengguna<br>

-   Tambah Data (Data Pengguna)<br>
-   Ubah Data (Data Pengguna)<br>
-   Hapus Data (Data Pengguna)<br>
-   Detail Data (Data Pengguna)<br>
-   Cari Data (Data Pengguna)<br>
-   Print Data (Data Pengguna)<br>
-   PDF Data (Data Pengguna)<br>
-   Print Data Detail (Data Pengguna)<br>
-   PDF Data Detail (Data Pengguna)<br>
-   Excel Export Data (Data Pengguna)<br><br>

    C. Master Barang<br>

-   Tambah Data (Master Barang)<br>
-   Ubah Data (Master Barang)<br>
-   Hapus Data (Master Barang)<br>
-   Detail Data (Master Barang)<br>
-   Cari Data (Master Barang)<br>
-   Print Data (Master Barang)<br>
-   PDF Data (Master Barang)<br>
-   Print Data Detail (Master Barang)<br>
-   PDF Data Detail (Master Barang)<br>
-   Excel Export Data (Master Barang)<br><br>

    C. Transaksi Pembelian<br>

-   Tambah Data (Transaksi Pembelian)<br>
-   Ubah Data (Transaksi Pembelian)<br>
-   Hapus Data (Transaksi Pembelian)<br>
-   Detail Data (Transaksi Pembelian)<br>
-   Cari Data (Transaksi Pembelian)<br>
-   Print Data (Transaksi Pembelian)<br>
-   PDF Data (Transaksi Pembelian)<br>
-   Print Data Detail (Transaksi Pembelian)<br>
-   PDF Data Detail (Transaksi Pembelian)<br>
-   Excel Export Data (Transaksi Pembelian)<br><br>

    C. Transaksi Pembelian Barang<br>

-   Tambah Data (Transaksi Pembelian Barang)<br>
-   Ubah Data (Transaksi Pembelian Barang)<br>
-   Hapus Data (Transaksi Pembelian Barang)<br>
-   Detail Data (Transaksi Pembelian Barang)<br>
-   Cari Data (Transaksi Pembelian Barang)<br>
-   Print Data (Transaksi Pembelian Barang)<br>
-   PDF Data (Transaksi Pembelian Barang)<br>
-   Print Data Detail (Transaksi Pembelian Barang)<br>
-   PDF Data Detail (Transaksi Pembelian Barang)<br>
-   Excel Export Data (Transaksi Pembelian Barang)<br><br>

## Aplikasi Screenshot

<img src="public/assets_readme/img/Halaman_Utama.PNG" alt="Halaman Utama">
<img src="public/assets_readme/img/Halaman_Utama_Setelah_Login.PNG" alt="Halaman Setalah Login">
<img src="public/assets_readme/img/Menu_Admin.PNG" alt="Menu Admin">
<img src="public/assets_readme/img/Menu_Kasir.PNG" alt="Menu Kasir">
<img src="public/assets_readme/img/Daftar_Transaksi.PNG" alt="Daftar Transaksi">
<img src="public/assets_readme/img/Harga_Total.PNG" alt="Harga Total">

## Alat Yang Digunakan Untuk Membuat Web :

A. Hardware :
Laptop Nitro 5 AN515-55. Speaksifikasi :

-   Ram : 8GB 
-   Prosesor : Intel Core i5-10300H CPU @ 2.50GHz (8 CPUs), ~2.5GHz
-   Tipe System : 64 bit
-   VGA : NVIDIA GEFORCE GTX 1650 Ti
-   SSD : 512 GB

B. Software :

-   XAMPP
-   Visual Studio Code
-   Git
-   PHP 8.0.7

C. Komponen

-   Laravel 8
-   Bootstrap 4 & 5
-   Template Tambahan :
    -   Sb Admin 2 (Bootstrap 4) (Untuk Template Admin Full)
    -   Zinc (Bootstrap 5) (Khusus Halaman Utama)
-   Library :
    -   Carbon Laravel Time
    -   Dompdf
    -   Laravel Excel
    -   Sweet Alert
    -   Datatables
    -   TinyMCE

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>
